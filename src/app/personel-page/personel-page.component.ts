import { Component, OnInit } from '@angular/core';
import { PersonelService } from "../service/personel.service";
import { Personel } from "../model/personel";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-personel-page',
  templateUrl: './personel-page.component.html',
  styleUrls: ['./personel-page.component.scss']
})
export class PersonelPageComponent implements OnInit {
  personels: Personel[];
  errorMessage: String;
  personel=new Personel();

  personelName: String;

  constructor(private _personelService: PersonelService) { 
  }


  ngOnInit(): void {
    this.fetchPersonel();
  }

  fetchPersonel(): void {
    this._personelService.getListPersonels()
      .subscribe(personels => this.personels = personels,
        error => this.errorMessage = <any>error
      );
    console.log(this.personels);
  }

  addPersonel() :void {

    this._personelService.AddListPersonel(this.personel)
      .subscribe(personel => {
        this.fetchPersonel();
        this.reset();
        this.personelName = personel.firstName;
      },
        error => this.errorMessage = <any>error);
        console.log(this.personel);
  }

  updatePersonel(personel:Personel):void{
    console.log("personel");
    console.log(personel);
    console.log("personel");
    this._personelService.putPersonel(personel)
    .subscribe(result=>console.log(result),result=>console.log("neOK")
    );
    console.log(personel);
  }

  dropPersonal(id:number){
    this._personelService.deletePersonal(id)
    .subscribe( personel=>{this.fetchPersonel(),res=>console.log("neOK")});
  }

 
  private reset() {
    this.personel.idPersonel = null;
    this.personel.firstName = null;
    this.personel.lastName = null;
    this.personel.post = null;

    this.errorMessage = null;
    this.personelName = null;
  }
}
