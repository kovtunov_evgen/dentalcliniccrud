import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonelPageComponent } from './personel-page.component';

describe('PersonelPageComponent', () => {
  let component: PersonelPageComponent;
  let fixture: ComponentFixture<PersonelPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonelPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonelPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
