import { Component, OnInit } from '@angular/core';
import { PatientService } from "../service/patient.service";
import { Patient } from "../model/patient";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-patient-page',
  templateUrl: './patient-page.component.html',
  styleUrls: ['./patient-page.component.scss']
})
export class PatientPageComponent implements OnInit {

  patients:Patient[];
  errorMessage: String;
  patient=new Patient();
  isSelected:boolean = false;

  patientName: String;
  constructor(private _patientService:PatientService) { }

  ngOnInit():void{
    this.fetchPatient();
  }

fetchPatient(): void {
    this._patientService.getListPatient()
      .subscribe(patients => this.patients = patients,
        error => this.errorMessage = <any>error
      );
    console.log(this.patients);
  }

  addPatient() :void {

    this._patientService.AddListPatient(this.patient)
      .subscribe(patient => {
        this.fetchPatient();
        this.reset();
        this.patientName = patient.firstName;
      },
        error => this.errorMessage = <any>error);
        console.log(this.patient);
  }

  updatePatient(patient:Patient):void{
    console.log("patient");
    console.log(patient);
    console.log("patient");
    this._patientService.putPatient(patient)
    .subscribe(result=>console.log(result),result=>console.log("neOK")
    );
    console.log(patient);
  }

  dropPatient(id:number){
    this._patientService.deletePatient(id)
    .subscribe( department=>{this.fetchPatient(),res=>console.log("neOK")});
  }

 
  private reset() {
    this.patient.idPatient = null;
    this.patient.firstName = null;
    this.patient.lastName = null;
    this.errorMessage = null;
    this.patientName = null;
  }
  onSelect(): void {
    this.isSelected = !this.isSelected;
  }

}
