import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientdiseasePageComponent } from './patientdisease-page.component';

describe('PatientdiseasePageComponent', () => {
  let component: PatientdiseasePageComponent;
  let fixture: ComponentFixture<PatientdiseasePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientdiseasePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientdiseasePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
