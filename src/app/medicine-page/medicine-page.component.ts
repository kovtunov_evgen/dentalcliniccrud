import { Component, OnInit } from '@angular/core';
import { MedicineService } from "../service/medicine.service";
import { Medicine } from "../model/medicine";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-medicine-page',
  templateUrl: './medicine-page.component.html',
  styleUrls: ['./medicine-page.component.scss']
})
export class MedicinePageComponent implements OnInit {

  medicines:Medicine[];
  errorMessage: String;
  medicine=new Medicine();

  medicineName: String;
  constructor(private _medicineService:MedicineService) { }

  ngOnInit():void{
    this.fetchMedicine();
  }

fetchMedicine(): void {
    this._medicineService.getListMedicine()
      .subscribe(medicines => this.medicines = medicines,
        error => this.errorMessage = <any>error
      );
    console.log(this.medicines);
  }

  addMedicine() :void {

    this._medicineService.AddListMedicine(this.medicine)
      .subscribe(medicine => {
        this.fetchMedicine();
        this.reset();
        this.medicineName = medicine.name;
      },
        error => this.errorMessage = <any>error);
        console.log(this.medicine);
  }

  updateMedicine(medicine:Medicine):void{
    console.log("medicine");
    console.log(medicine);
    console.log("medicine");
    this._medicineService.putMedicine(medicine)
    .subscribe(result=>console.log(result),result=>console.log("neOK")
    );
    console.log(medicine);
  }

  dropMedicine(id:number){
    this._medicineService.deleteMedicine(id)
    .subscribe( medicine=>{this.fetchMedicine(),res=>console.log("neOK")});
  }

 
  private reset() {
    this.medicine.idMedicine = null;
    this.medicine.name = null;
    this.medicine.ageLimit = null;

    this.errorMessage = null;
    this.medicineName = null;
  }

}
