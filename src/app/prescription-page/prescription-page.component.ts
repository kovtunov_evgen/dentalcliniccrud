import { Component, OnInit } from '@angular/core';
import { PrescriptionService } from "../service/prescription.service";
import { Prescription } from "../model/prescription";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-prescription-page',
  templateUrl: './prescription-page.component.html',
  styleUrls: ['./prescription-page.component.scss']
})
export class PrescriptionPageComponent implements OnInit {

  prescriptions:Prescription[];
  errorMessage: String;
  prescription=new Prescription();

  prescriptionName: String;
  constructor(private _prescriptionService:PrescriptionService) { }

  ngOnInit():void{
    this.fetchPrescription();
  }

fetchPrescription(): void {
    this._prescriptionService.getListPrescription()
      .subscribe(prescriptions => this.prescriptions = prescriptions,
        error => this.errorMessage = <any>error
      );
    console.log(this.prescriptions);
  }

  addPrescription() :void {

    this._prescriptionService.AddListPrescription(this.prescription)
      .subscribe(prescription => {
        this.fetchPrescription();
        this.reset();
       // this.prescriptionName = prescription.idPrescription;
      },
        error => this.errorMessage = <any>error);
        console.log(this.prescription);
  }

  updateDepartment(prescription:Prescription):void{
    console.log("prescription");
    console.log(prescription);
    console.log("prescription");
    this._prescriptionService.putPrescription(prescription)
    .subscribe(result=>console.log(result),result=>console.log("neOK")
    );
    console.log(prescription);
  }

  dropPersonal(id:number){
    this._prescriptionService.deletePrescription(id)
    .subscribe( prescription=>{this.fetchPrescription(),res=>console.log("neOK")});
  }

 
  private reset() {
    this.prescription.idPrescription = null;
    this.prescription.medicine = null;
    this.prescription.patient = null;

    this.errorMessage = null;
    this.prescriptionName = null;
  }

}
