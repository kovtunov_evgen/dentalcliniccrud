import { Component, OnInit } from '@angular/core';
import { HospitalService } from "../service/hospital.service"
import { Hospital } from "../model/hospital";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-hospital-page',
  templateUrl: './hospital-page.component.html',
  styleUrls: ['./hospital-page.component.scss']
})
export class HospitalPageComponent implements OnInit {
  hospitals: Hospital[];
  errorMessage: String;
  hospital=new Hospital();
  isSelected:boolean = false;

  hospitalName: String;
 
  constructor(private hospitalService:HospitalService) { }

  ngOnInit():void {
    this.fetchHospital();
  }
  fetchHospital(): void {
    this.hospitalService.getListHospital()
      .subscribe(hospitals => this.hospitals = hospitals,
        error => this.errorMessage = <any>error
      );
    console.log(this.hospitals);
  }

  addHospital() :void {

    this.hospitalService.AddListHospital(this.hospital)
      .subscribe(hospital => {
        this.fetchHospital();
        this.reset();
        this.hospitalName = hospital.city;
      },
        error => this.errorMessage = <any>error);
        console.log(this.hospital);
  }

  updateHospital(hospital:Hospital):void{
    console.log("hospital");
    console.log(hospital);
    console.log("hospital");
    this.hospitalService.putHospital(hospital)
    .subscribe(result=>console.log(result),result=>console.log("neOK")
    );
    console.log(hospital);
  }

  dropHospital(id:number){
    this.hospitalService.deleteHospital(id)
    .subscribe( hospital=>{this.fetchHospital(),res=>console.log("neOK")});
  }

 
  private reset() {
    this.hospital.idHospital = null;
    this.hospital.city = null;
    this.hospital.description = null;
    this.errorMessage = null;
    this.hospitalName = null;
  }
  onSelect(): void {
    this.isSelected = !this.isSelected;
  }
}
