import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from "@angular/http"
import { AppComponent } from './app.component';
import { HospitalService } from "./service/hospital.service";
import { PersonelService } from "./service/personel.service";
import { PatientService } from "./service/patient.service";
import { DiseaseService } from "./service/disease.service";
import { MedicineService } from "./service/medicine.service";
import { PrescriptionService } from "./service/prescription.service";
import { DepartmentService } from "./service/department.service";
import { TestComponent } from './test/test.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HospitalPageComponent } from './hospital-page/hospital-page.component';
import { PersonelPageComponent } from './personel-page/personel-page.component';
import { ReactiveFormsModule, FormControl, FormsModule } from '@angular/forms';
import { DepertmentPageComponent } from './depertment-page/depertment-page.component';
import { PatientPageComponent } from './patient-page/patient-page.component';
import { DiseasePageComponent } from './disease-page/disease-page.component';
import { MedicinePageComponent } from './medicine-page/medicine-page.component';
import { PatientdiseasePageComponent } from './patientdisease-page/patientdisease-page.component';
import { PrescriptionPageComponent } from './prescription-page/prescription-page.component';
import { ServicePageComponent } from './service-page/service-page.component';
import { TreatmentPageComponent } from './treatment-page/treatment-page.component';
@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    HomePageComponent,
    NavbarComponent,
    HospitalPageComponent,
    PersonelPageComponent,
    DepertmentPageComponent,
    PatientPageComponent,
    DiseasePageComponent,
    MedicinePageComponent,
    PatientdiseasePageComponent,
    PrescriptionPageComponent,
    ServicePageComponent,
    TreatmentPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    HttpModule,
    FormsModule,
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [HospitalService, PersonelService, DepartmentService, PatientService, DiseaseService, MedicineService, PrescriptionService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
