import { Injectable } from '@angular/core';
import { Disease } from '../model/disease';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { Headers, RequestOptions } from '@angular/http';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class DiseaseService {


  constructor(private _http: Http) { }

  getListDisease(): Observable<Disease[]> {
   return this._http.get("http://localhost:52652/api/disease")
     .map(this.extractData)
   .catch(this.handleErrorObservable);
 }



AddListDisease(disease: Disease): Observable<Disease> {

   let headers = new Headers({ 'Content-Type': 'application/json' });
   headers.append('Access-Control-Allow-Origin','*');
   

   let options = new RequestOptions({ headers: headers });
  return this._http.post("http://localhost:52652/api/disease", disease, options)
    .map(this.extractData)
     .catch(this.handleErrorObservable);
 }

 putDisease(disease: any): Observable<Disease> {
   let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
  console.log(disease);
  return this._http.put("http://localhost:52652/api/disease/"+disease.idDisease, disease, options)
    .map(this.extractData)
   .catch(this.handleErrorObservable);
     
 }

 deleteDisease(id: number): Observable<Disease> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
   return this._http.delete("http://localhost:52652/api/disease/" + id, options)
     .map(success => success.status)
    .catch(this.handleError);
 }

 

 extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

 private handleErrorObservable(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.message || error);
}

 private handleError(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.status);
 }

}
