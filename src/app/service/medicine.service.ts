import { Injectable } from '@angular/core';
import { Medicine } from '../model/medicine';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { Headers, RequestOptions } from '@angular/http';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class MedicineService {


  constructor(private _http: Http) { }

  getListMedicine(): Observable<Medicine[]> {
   return this._http.get("http://localhost:52652/api/medicine")
     .map(this.extractData)
   .catch(this.handleErrorObservable);
 }



AddListMedicine(medicine: Medicine): Observable<Medicine> {

   let headers = new Headers({ 'Content-Type': 'application/json' });
   headers.append('Access-Control-Allow-Origin','*');
   

   let options = new RequestOptions({ headers: headers });
  return this._http.post("http://localhost:52652/api/medicine", medicine, options)
    .map(this.extractData)
     .catch(this.handleErrorObservable);
 }

 putMedicine(medicine: any): Observable<Medicine> {
   let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
  console.log(medicine);
  return this._http.put("http://localhost:52652/api/medicine/"+medicine.idMedicine, medicine, options)
    .map(this.extractData)
   .catch(this.handleErrorObservable);
     
 }

 deleteMedicine(id: number): Observable<Medicine> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
   return this._http.delete("http://localhost:52652/api/medicine/" + id, options)
     .map(success => success.status)
    .catch(this.handleError);
 }

 

 extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

 private handleErrorObservable(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.message || error);
}

 private handleError(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.status);
 }

}
