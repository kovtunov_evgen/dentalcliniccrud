import { Injectable } from '@angular/core';
import { Department } from '../model/department';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { Headers, RequestOptions } from '@angular/http';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class DepartmentService {


  constructor(private _http: Http) { }

  getListDepartment(): Observable<Department[]> {
   return this._http.get("http://localhost:52652/api/department")
     .map(this.extractData)
   .catch(this.handleErrorObservable);
 }



AddListDepartment(department: Department): Observable<Department> {

   let headers = new Headers({ 'Content-Type': 'application/json' });
   headers.append('Access-Control-Allow-Origin','*');
   

   let options = new RequestOptions({ headers: headers });
  return this._http.post("http://localhost:52652/api/department", department, options)
    .map(this.extractData)
     .catch(this.handleErrorObservable);
 }

 putDepartment(department: any): Observable<Department> {
   let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
  console.log(department);
  return this._http.put("http://localhost:52652/api/department/"+department.idDepartment, department, options)
    .map(this.extractData)
   .catch(this.handleErrorObservable);
     
 }

 deleteDepartment(id: number): Observable<Department> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
   return this._http.delete("http://localhost:52652/api/department/" + id, options)
     .map(success => success.status)
    .catch(this.handleError);
 }

 

 extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

 private handleErrorObservable(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.message || error);
}

 private handleError(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.status);
 }

}
