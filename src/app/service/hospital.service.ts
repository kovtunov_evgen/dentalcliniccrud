import { Injectable } from '@angular/core';
import { Hospital } from "../model/hospital";
import { Http,Response } from "@angular/http";
import { Observable } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { URLSearchParams } from '@angular/http';
@Injectable()
export class HospitalService {
  hospitals: Hospital[];
 
  constructor(private _http: Http) {

  }

  getListHospital(): Observable<Hospital[]> {
    return this._http.get("http://localhost:52652/api/hospital")
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }



  AddListHospital(hospital: Hospital): Observable<Hospital> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin','*');
   

    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:52652/api/hospital", hospital, options)
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  putHospital(hospital: any): Observable<Hospital> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log(hospital);
    return this._http.put("http://localhost:52652/api/hospital/"+hospital.idHospital, hospital, options)
      .map(this.extractData)
      .catch(this.handleErrorObservable);
     
  }

  deleteHospital(id: number): Observable<Hospital> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.delete("http://localhost:52652/api/hospital/" + id, options)
      .map(success => success.status)
      .catch(this.handleError);
  }

 

  extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleErrorObservable(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  private handleError(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.status);
  }


}
