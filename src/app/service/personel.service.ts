import { Injectable } from '@angular/core';
import { Personel } from '../model/personel';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { Headers, RequestOptions } from '@angular/http';
import { URLSearchParams } from '@angular/http';
@Injectable()
export class PersonelService {
  //personels:Personel[];

  constructor(private _http: Http) { }

  getListPersonels(): Observable<Personel[]> {
    return this._http.get("http://localhost:52652/api/personel")
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }



  AddListPersonel(personel: Personel): Observable<Personel> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin','*');
   

    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:52652/api/personel", personel, options)
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  putPersonel(personel: any): Observable<Personel> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log(personel);
    return this._http.put("http://localhost:52652/api/personel/"+personel.idPersonel, personel, options)
      .map(this.extractData)
      .catch(this.handleErrorObservable);
     
  }

  deletePersonal(id: number): Observable<Personel> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.delete("http://localhost:52652/api/personel/" + id, options)
      .map(success => success.status)
      .catch(this.handleError);
  }

 

  extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleErrorObservable(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  private handleError(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.status);
  }

}
