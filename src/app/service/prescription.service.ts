import { Injectable } from '@angular/core';
import { Prescription } from '../model/prescription';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { Headers, RequestOptions } from '@angular/http';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class PrescriptionService {


  constructor(private _http: Http) { }

  getListPrescription(): Observable<Prescription[]> {
   return this._http.get("http://localhost:52652/api/prescription")
     .map(this.extractData)
   .catch(this.handleErrorObservable);
 }



AddListPrescription(prescription: Prescription): Observable<Prescription> {

   let headers = new Headers({ 'Content-Type': 'application/json' });
   headers.append('Access-Control-Allow-Origin','*');
   

   let options = new RequestOptions({ headers: headers });
  return this._http.post("http://localhost:52652/api/prescription", prescription, options)
    .map(this.extractData)
     .catch(this.handleErrorObservable);
 }

 putPrescription(prescription: any): Observable<Prescription> {
   let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
  console.log(prescription);
  return this._http.put("http://localhost:52652/api/prescription/"+prescription.idPrescription, prescription, options)
    .map(this.extractData)
   .catch(this.handleErrorObservable);
     
 }

 deletePrescription(id: number): Observable<Prescription> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
   return this._http.delete("http://localhost:52652/api/prescription/" + id, options)
     .map(success => success.status)
    .catch(this.handleError);
 }

 

 extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

 private handleErrorObservable(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.message || error);
}

 private handleError(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.status);
 }

}
