import { Injectable } from '@angular/core';
import { Patient } from '../model/patient';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { Headers, RequestOptions } from '@angular/http';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class PatientService {


  constructor(private _http: Http) { }

  getListPatient(): Observable<Patient[]> {
   return this._http.get("http://localhost:52652/api/patient")
     .map(this.extractData)
   .catch(this.handleErrorObservable);
 }



AddListPatient(patient: Patient): Observable<Patient> {

   let headers = new Headers({ 'Content-Type': 'application/json' });
   headers.append('Access-Control-Allow-Origin','*');
   

   let options = new RequestOptions({ headers: headers });
  return this._http.post("http://localhost:52652/api/patient", patient, options)
    .map(this.extractData)
     .catch(this.handleErrorObservable);
 }

 putPatient(patient: any): Observable<Patient> {
   let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
  console.log(patient);
  return this._http.put("http://localhost:52652/api/patient/"+patient.idPatient, patient, options)
    .map(this.extractData)
   .catch(this.handleErrorObservable);
     
 }

 deletePatient(id: number): Observable<Patient> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
   let options = new RequestOptions({ headers: headers });
   return this._http.delete("http://localhost:52652/api/patient/" + id, options)
     .map(success => success.status)
    .catch(this.handleError);
 }

 

 extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

 private handleErrorObservable(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.message || error);
}

 private handleError(error: Response | any) {
   console.error(error.message || error);
   return Observable.throw(error.status);
 }

}
