import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepertmentPageComponent } from './depertment-page.component';

describe('DepertmentPageComponent', () => {
  let component: DepertmentPageComponent;
  let fixture: ComponentFixture<DepertmentPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepertmentPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepertmentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
