import { Component, OnInit } from '@angular/core';
import { DepartmentService } from "../service/department.service";
import { Department } from "../model/department";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-depertment-page',
  templateUrl: './depertment-page.component.html',
  styleUrls: ['./depertment-page.component.scss']
})
export class DepertmentPageComponent implements OnInit {
 departments:Department[];
  errorMessage: String;
  department=new Department();
  isSelected:boolean = false;

  departmentName: String;
  constructor(private _departmentService:DepartmentService) { }

  ngOnInit():void{
    this.fetchDepartment();
  }

fetchDepartment(): void {
    this._departmentService.getListDepartment()
      .subscribe(departments => this.departments = departments,
        error => this.errorMessage = <any>error
      );
    console.log(this.departments);
  }

  addDepartment() :void {

    this._departmentService.AddListDepartment(this.department)
      .subscribe(department => {
        this.fetchDepartment();
        this.reset();
        this.departmentName = department.name;
      },
        error => this.errorMessage = <any>error);
        console.log(this.department);
  }

  updateDepartment(department:Department):void{
    console.log("department");
    console.log(department);
    console.log("department");
    this._departmentService.putDepartment(department)
    .subscribe(result=>console.log(result),result=>console.log("neOK")
    );
    console.log(department);
  }

  dropPersonal(id:number){
    this._departmentService.deleteDepartment(id)
    .subscribe( department=>{this.fetchDepartment(),res=>console.log("neOK")});
  }

 
  private reset() {
    this.department.idDepartment = null;
    this.department.name = null;

    this.errorMessage = null;
    this.departmentName = null;
  }
  onSelect(): void {
    this.isSelected = !this.isSelected;
  }

}
