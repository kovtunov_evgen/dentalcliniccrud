import { Component, OnInit } from '@angular/core';
import { DiseaseService } from "../service/disease.service";
import { Disease } from "../model/disease";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-disease-page',
  templateUrl: './disease-page.component.html',
  styleUrls: ['./disease-page.component.scss']
})
export class DiseasePageComponent implements OnInit {

  diseases:Disease[];
  errorMessage: String;
  disease=new Disease();
  isSelected:boolean = false;

  diseaseName: String;
  constructor(private _diseaseService:DiseaseService) { }

  ngOnInit():void{
    this.fetchDisease();
  }

fetchDisease(): void {
    this._diseaseService.getListDisease()
      .subscribe(diseases => this.diseases = diseases,
        error => this.errorMessage = <any>error
      );
    console.log(this.diseases);
  }

  addDisease() :void {

    this._diseaseService.AddListDisease(this.disease)
      .subscribe(disease => {
        this.fetchDisease();
        this.reset();
        this.diseaseName = disease.name;
      },
        error => this.errorMessage = <any>error);
        console.log(this.disease);
  }

  updateDisease(disease:Disease):void{
    console.log("disease");
    console.log(disease);
    console.log("disease");
    this._diseaseService.putDisease(disease)
    .subscribe(result=>console.log(result),result=>console.log("neOK")
    );
    console.log(disease);
  }

  dropDisease(id:number){
    this._diseaseService.deleteDisease(id)
    .subscribe( department=>{this.fetchDisease(),res=>console.log("neOK")});
  }

 
  private reset() {
    this.disease.idDisease = null;
    this.disease.description = null;
    this.disease.name = null;

    this.errorMessage = null;
    this.diseaseName = null;
  }
  onSelect(): void {
    this.isSelected = !this.isSelected;
  }

}
