import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { TestComponent } from './test/test.component';
import { HospitalPageComponent } from './hospital-page/hospital-page.component';
import { PersonelPageComponent } from './personel-page/personel-page.component';
import { DepertmentPageComponent } from './depertment-page/depertment-page.component';
import { PatientPageComponent } from './patient-page/patient-page.component';
import { DiseasePageComponent } from './disease-page/disease-page.component';
import { MedicinePageComponent } from './medicine-page/medicine-page.component';
import { PatientdiseasePageComponent } from './patientdisease-page/patientdisease-page.component';
import { PrescriptionPageComponent } from './prescription-page/prescription-page.component';
import { ServicePageComponent } from './service-page/service-page.component';
import { TreatmentPageComponent } from './treatment-page/treatment-page.component';

const routes: Routes = [
 { path:"home",component:HomePageComponent},
 { path:"hospital",component:HospitalPageComponent},
 { path:"personel",component:PersonelPageComponent},
 { path:"department",component:DepertmentPageComponent},
 { path:"patient",component:PatientPageComponent},
 { path:"disease",component:DiseasePageComponent},
 { path:"medicine",component:MedicinePageComponent},
 { path:"patientDisease",component:PatientdiseasePageComponent},
 { path:"prescription",component:PrescriptionPageComponent},
 { path:"service",component:ServicePageComponent},
 { path:"treatment",component:TreatmentPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
