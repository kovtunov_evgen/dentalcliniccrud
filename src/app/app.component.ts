import { Component } from '@angular/core';
import { HospitalService } from "./service/hospital.service"
import { Hospital } from "./model/hospital";
import { Http } from "@angular/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
export class AppComponent {
 
  constructor(private hospitalService: HospitalService) {

  }
}
